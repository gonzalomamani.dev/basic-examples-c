/**
 * 7.- la calficación final de una estudiante es el promedio de tres notas:
 *  - La nota de prácticas que cuenta un 30% del total
 *  - La nota teórica que cuenta un 60%
 *  - La nota de participación que cuenta el 10% restante.
 * Escriba un programa que lea las tres notas del alumno y escriba su nota final.
*/
#include <iostream>
using namespace std;

int main() {
    float practica, teorica, participacion, notaFinal;
    cout<<"Ingrese la nota practica del alumno: ";cin>>practica;
    cout<<"Ingrese la nota teórica del alumno: ";cin>>teorica;
    cout<<"Ingrese la nota de participacion del alumno: "; cin>>participacion;
    practica *= 0.30;
    teorica *= 0.60;
    participacion *= 0.10;
    notaFinal = practica + teorica + participacion;
    cout<<"\nLa nota final es: "<<notaFinal<<endl;
    return 0;
}