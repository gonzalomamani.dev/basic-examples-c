/**
 * Escriba un programa que lea la nota final de cuatro alumnos y calcule la nota final media de los cuatro alumnos.
 */

#include <iostream>

using namespace std;

int main() {
    float a1, a2, a3, a4, notaFinal;
    cout<<"Ingrese la nota del primer alumno: ";cin>>a1;
    cout<<"Ingrese la nota de segundo alumno: ";cin>>a2;
    cout<<"Ingrese la nota del tercer alumno: ";cin>>a3;
    cout<<"Ingrese la nota del cuarto alumno: ";cin>>a4;
    notaFinal = (a1+a2+a3+a4)/4;
    cout.precision(2);
    cout<<"\nLa nota media de los alumnos es: "<<notaFinal<<endl;
    return 0;
}

