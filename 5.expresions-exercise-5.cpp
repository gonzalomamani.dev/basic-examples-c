/**
 * Escriba un fragmento de programa que intercambie los valores de dos variables.
 */
#include <iostream>
using namespace std;

int main() {
    float a, b, aux;
    cout<<"Ingrese el valor de a: "; cin>>a;
    cout<<"Ingrese el valor de b: "; cin>>b;
    cout<<"\nEl valor de a es: "<<a<<endl;
    cout<<"\nEl valor de b es: "<<b<<endl;
    aux = a;
    a = b;
    b = aux;
    cout<<"\nEl nuevo valor de a es: "<<a<<endl;
    cout<<"\nEl nuevo valor de b es: "<<b<<endl;
    return 0;
}
