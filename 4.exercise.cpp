/**
 * 2.Escribe un programa que leade la entrada estandar el precio de un producto y muestre en la salida estadar
 * el precio del producto al aplicarle el IVA
 */
#include "iostream"
using namespace std;
int main(){
    float numero, iva = 0.13, precio=0;
    cout<<"Ingrese un el precio del producto por favor: ";
    cin>>numero;
    precio = numero + (numero*iva);
    cout<<"\nEl precio ingresado es: "<<numero;
    cout<<"\nEl iva es: "<<iva;
    cout<<"\nEl precio aplicado el IVA es: "<<precio;
    return 0;
}