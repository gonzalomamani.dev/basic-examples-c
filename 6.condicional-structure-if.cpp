/**
 * Usando las estructura condicional IF
*/
#include <iostream>
using namespace std;

int main(){
    int numero, dato = 5;
    cout<<"Ingrese el una valor numérico: "; cin>>numero;
    cout<<"--------- Evaluando la igualdad (=) ----------";
    if (numero == dato) {
        cout<<"\nEl número es igual a 5";
    } else {
        cout<<"\nEl número no es igual a 5";
    }
    cout<<"\n--------- Evaluando la diferencia (!=) ----------";
    if (numero != dato) {
        cout<<"\nEl número es diferente a 5";
    } else {
        cout<<"\nEl número no es diferente a 5";
    }
    cout<<"\n--------- Evaluando si es mayor (>) ----------";
    if (numero > dato) {
        cout<<"\nEl número es mayor a 5";
    } else {
        cout<<"\nEl número no es mayor a 5";
    }
    cout<<"\n--------- Evaluando si es menor (<) ----------";
    if (numero < dato) {
        cout<<"\nEl número es menor a 5";
    } else {
        cout<<"\nEl número no es menor a 5";
    }
    cout<<"\n--------- Evaluando si es menor o igual (>=) ----------";
    if (numero >= dato) {
        cout<<"\nEl número es mayor o igual a 5";
    } else {
        cout<<"\nEl número no es mayor o igual a 5";
    }
    cout<<"\n--------- Evaluando si es menor o igual(<=) ----------";
    if (numero <= dato) {
        cout<<"\nEl número es menor o igual a 5";
    } else {
        cout<<"\nEl número no es menor o igual a 5";
    }
    return 0;
}