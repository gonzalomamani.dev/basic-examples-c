/**
 * Escriba un programa que escriba las soluciones de una ecuación de segundo grado de la forma ax²+bx+c
 * teniendo en cuenta que: x = (-b+-\/b²-4ac)/2a
*/
#include <iostream>
#include <math.h>
using namespace std;
int main() {
    float a, b, c, x, resultado1, resultado2;
    cout<<"Ingrese el valor de a: "; cin>>a;
    cout<<"Ingrese el valor de b: "; cin>>b;
    cout<<"Ingrese el valor de c: "; cin>>c;
    float raiz = sqrt(pow(b,2) - 4 * a * c);
    resultado1 = (-b+(sqrt(pow(b,2) - 4 * a * c)))/2*a;
    resultado2 = (-b-(sqrt(pow(b,2) - 4 * a * c)))/2*a;
    cout.precision(2);
    cout<<"\nEl resultado 1 es: "<<resultado1<<endl;
    cout<<"\nEl resultado 2 es: "<<resultado2<<endl;
    return 0;
}