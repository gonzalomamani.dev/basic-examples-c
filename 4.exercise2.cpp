/**
 * 3. Realice un programa que lea de la entrada estandar los siguientes datos de una persona
 * Edad: dato de tipo entero
 * Sexo: dato de tipo caracter
 * Altura en metros: dato de tipo real
 * Tras leer los datos, el programa debe de mostrarlos en la salida estandar
 */
#include "iostream"
using namespace std;
int main() {
    int edad;
//    string sexo;
    char sexo[10];
    float altura;
    cout<<"Ingresa tu edad: ";
    cin>>edad;
    cout<<"Ingresa tu sexo: ";
    cin>>sexo;
    cout<<"Ingresa tu altura en metros: ";
    cin>>altura;
    cout<<"\nTu edad es: "<<edad;
    cout<<"\nTu sexo es: "<<sexo;
    cout<<"\nTu altura en metros es: "<<altura<<endl;
    return 0;
}

