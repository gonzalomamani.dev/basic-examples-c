#include <iostream>
using namespace std;
/**
 * Este es un ejemplo de la lectura de la entrada de datos y el manejo de comentarios
 * @return
 */
int main() {
    int numero;
    //Mostrando el mensaje al usuario
    cout<<"Digite un número: ";
    //Almacenando el número ingresado por el usuario
    cin>>numero;
    //Mostrando el número ingresado
    cout<<"\nEl número ingresado es: "<<numero;
    return 0;
}

