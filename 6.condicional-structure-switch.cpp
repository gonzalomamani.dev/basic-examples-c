/**
 * Haciendo uso de sentencia condicionante SWITCH
 */
#include <iostream>
using namespace std;
int main() {
    int numero;
    cout<<"Ingrese un valor entre 1-5: "; cin>>numero;
    switch (numero) {
        case 1: cout<<"\n caso 1"; break;
        case 2: cout<<"\n caso 2"; break;
        case 3: cout<<"\n caso 3"; break;
        case 4: cout<<"\n caso 4"; break;
        case 5: cout<<"\n caso 5"; break;
        default: cout<<"\n El número ingresado no esta entre 1-5";
    }
    return 0;
}
