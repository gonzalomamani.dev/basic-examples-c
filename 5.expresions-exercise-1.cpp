/**
 * 1. Escribe la siguiente espresion en c++
 *  a/b+1
 */
#include <iostream>
using namespace std;

int main() {
    float a, b, resultado = 0;
    cout<<"Ingrese el valor de a: ";
    cin>>a;
    cout<<"Ingrese el valor de b: ";
    cin>>b;
    resultado = (a/b)+1;
    cout.precision(3);//muestra solo dos dígitos
    cout<<"\nEl resultado es: "<<resultado<<endl;
    return 0;
}
