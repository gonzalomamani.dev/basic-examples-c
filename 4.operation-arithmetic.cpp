/**
 * 1. Escribe un programa que lea la entrada estandar dos número y muestre de salida
 * la suma, resta, multiplicación y división
 */
#include "iostream"
using namespace std;
int main() {
    int numero1, numero2, suma = 0, resta = 0, multiplicacion = 0, division = 0;
    cout<<"Ingrese el primer número: ";
    cin>>numero1;
    cout<<"Ingrese el segundo número: ";
    cin>>numero2;
    suma = numero1 + numero2;
    resta = numero1 - numero2;
    multiplicacion = numero1 * numero2;
    division = numero1 / numero2;
    cout<<"\nLa suma es: "<<suma;
    cout<<"\nLa resta es: "<<resta;
    cout<<"\nLa multiplicación es: "<<multiplicacion;
    cout<<"\nLa división es: "<<division;
    return 0;
}

