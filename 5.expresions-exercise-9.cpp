/**
 * Realice un programa que calcule el valor que toma la siguiente función dados los valores de x e y
 * f(x,y) = [\/x] / [(y^2)-1]
*/
#include <iostream>
#include <math.h>
using namespace std;
int main() {
    float x, y, resultado;
    cout<<"Ingrese el valor de x: "; cin>>x;
    cout<<"Ingrese el valos de y: "; cin>>y;
    resultado = sqrt(x)/(pow(y, 2) - 1);
    cout.precision(2);
    cout<<"El resultado es: "<<resultado<<endl;
    return 0;
}