/**
 * Escriba un programa que lea de la entrada estandar los dos catetos de un triangulo y
 * escriba en la salida estándar su hipotenusa.
*/
#include <iostream>
#include <math.h>
using namespace std;
int main() {
    float a, b, hipotenusa;
    cout<<"Ingrese el valor de a (cateto): "; cin>>a;
    cout<<"Ingrese el valor de b (cateto): "; cin>>b;
    hipotenusa = sqrt(pow(a, 2) + pow(b,2));
    cout.precision(2);
    cout<<"La hipotenusa es: "<<hipotenusa<<endl;
    return 0;
}